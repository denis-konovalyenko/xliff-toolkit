<?xml version="1.0"?> 
<project name="okapi-xlifflib" default="all" basedir="."> 

	<!-- Get the version information to use from file -->
	<property file="../shared/release.properties" />
	<property name="xlifflibStamp" value="${xlifflibVersion}" />
	<property name="mavenStore" value="${user.home}/.m2/repository"/>

 	<property name="tmp" value="tmp"/>
	<property name="done" value="done"/>
	<property name="dist_xlifflib" value="dist_xlifflib"/>
	<property name="dist_conf" value="${dist_xlifflib}/conformance"/>
	<property name="lib_name" value="okapi-lib-xliff2"/>
	<property name="lynx_name" value="lynx"/>
	<property name="distName" value="${lib_name}-${xlifflibStamp}"/>
	<property name="zipName" value="okapi-xliffLib_all-platforms_${xlifflibStamp}"/>

	<tstamp>
	 <format property="TODAY" pattern="MMM-dd-yyyy"/>
	</tstamp>

	<!-- Initialization -->
	<target name="init">
		<delete includeEmptyDirs="true" failonerror="false">
			<fileset dir="${dist_xlifflib}"/>
		</delete>
		<mkdir dir="${dist_xlifflib}"/>
		<delete includeEmptyDirs="true" failonerror="false">
			<fileset dir="${tmp}"/>
		</delete>
		<mkdir dir="${tmp}"/>
		<mkdir dir="${done}"/>
	</target>

	<!-- Make the jar files -->
	<target name="makeJars"> 

	<!-- Make a temporary copy of the manifest to replace the version info -->
	<copy file="../../okapi/libraries/lib-xliff/META-INF/MANIFEST.MF" tofile="${tmp}/MANIFEST.MF"/>
	<replace file="${tmp}/MANIFEST.MF" token="@version@" value="${xlifflibStamp} (${TODAY})"/>
	
	<jar destfile="${dist_xlifflib}/${distName}.jar"
		manifest="${tmp}/MANIFEST.MF">
		
		<fileset dir="../../okapi/libraries/lib-xliff/target/classes" includes="*.xsd"/>
		<fileset dir="../../okapi/libraries/lib-xliff/target/classes" includes="net/**"/>
		<fileset dir="../../okapi/libraries/lib-xliff/target/classes" includes="informativeCopiesOf3rdPartySchemas/**"/>
		<fileset dir="../../okapi/libraries/lib-xliff/target/classes" includes="modules/**"/>
		<fileset dir="../../okapi/libraries/lib-xliff/target/classes" includes="modules.xml"/>
		<fileset dir="../../okapi/libraries/lib-xliff/target/classes" includes="prefixes.properties"/>
		<fileset dir="../../okapi/libraries/lib-xliff/target/classes" includes="language-subtag-registry.txt"/>
		
	</jar>
	
	<!-- Copy and update the readme -->
	<copy tofile="${dist_xlifflib}/readme.html" file="data/xliff-lib/readme.html"/>
	<replace file="${dist_xlifflib}/readme.html" token="@version@" value="${xlifflibStamp}"/>
	<replace file="${dist_xlifflib}/readme.html" token="@date@" value="${TODAY}"/>
	<!-- Copy and update the change log -->
	<copy tofile="${dist_xlifflib}/changes.html" file="data/xliff-lib/changes.html"/>
	<replace file="${dist_xlifflib}/changes.html" token="@version@" value="${xlifflibStamp}"/>
	<replace file="${dist_xlifflib}/changes.html" token="@date@" value="${TODAY}"/>
	<!-- Copy and update the conformance notes -->
	<copy tofile="${dist_conf}/conformance-notes.html" file="data/xliff-lib/conformance-notes.html"/>
	<replace file="${dist_conf}/conformance-notes.html" token="@version@" value="${xlifflibStamp}"/>
	<replace file="${dist_conf}/conformance-notes.html" token="@date@" value="${TODAY}"/>
	<!-- Copy the conformance test files -->
	<copy todir="${dist_conf}/in-out">
		<fileset dir="../../applications/integration-tests/src/test/resources/in-out"/>
	</copy>
	<copy todir="${dist_conf}/invalid">
		<fileset dir="../../okapi/libraries/lib-xliff/src/test/resources/invalid"/>
	</copy>
	<copy todir="${dist_conf}/valid">
		<fileset dir="../../okapi/libraries/lib-xliff/src/test/resources/valid"/>
	</copy>
	<copy file="../../okapi/libraries/lib-xliff/src/test/resources/extra-prefixes.properties"
		todir="${dist_conf}/valid"/>
	<!-- Copy samples -->
	<copy todir="${dist_xlifflib}/samples">
    	<fileset dir="data/xliff-lib/samples"/>
  	</copy>
	<!-- Copy code example -->
	<copy todir="${dist_xlifflib}/code-example">
    	<fileset dir="data/example"  includes="pom.xml"/>
    	<fileset dir="data/example"  includes="src/**"/>
  	</copy>
  	
	<!-- javadoc files -->
	<javadoc destdir="${dist_xlifflib}/documentation" Encoding="UTF-8"
	 overview="../../okapi/libraries/lib-xliff/src/main/java/overview.html"
	 Windowtitle="Okapi XLIFF Toolkit"
	>
		<packageset dir="../../okapi/libraries/lib-xliff/src/main/java" defaultexcludes="yes"/>
	</javadoc>

	</target>

	<!-- Make Lynx -->
	<target name="makeLynx">
		<!-- Prepare tmp dir for creating the jar -->
		<delete includeEmptyDirs="true" failonerror="false">
			<fileset dir="${tmp}"/>
		</delete>
		<mkdir dir="${tmp}"/>

		<!-- Copy classes -->
		<copy todir="${tmp}">
			<fileset dir="../../applications/lynx/target/classes"/>
		</copy>
		
		<!-- Create the manifest file -->
		<manifest file="${tmp}/MANIFEST.MF">
			<attribute name="Implementation-Title" value="lynx"/>
			<attribute name="Implementation-Version" value="${xlifflibStamp}"/>
			<attribute name="Main-Class" value="net.sf.okapi.applications.lynx.Main"/>
			<attribute name="Class-Path" value="${distName}.jar"/>
			<attribute name="Permissions" value="all-permissions"/>
  		</manifest>
		
		<!-- Create the jar file -->
		<jar jarfile="${dist_xlifflib}/lynx.jar" basedir="${tmp}"
		 manifest="${tmp}/MANIFEST.MF"
		 excludes="MANIFEST.MF">
			<fileset dir="data/xliff-lib" includes="**/*.txt"/>
			<fileset dir="../../applications/lynx/src/main/java/net/sf/okapi/applications/lynx" includes="Snippets.java"/>
		</jar>

		<!-- Copy batch/shell -->
		<copy todir="${dist_xlifflib}">
			<fileset dir="data/xliff-lib" includes="**/lynx*" excludes="**/lynx.jnlp"/>
			<fileset dir="data/xliff-lib" includes="**/startLynx*"/>
			<fileset dir="data/xliff-lib" includes="**/license.txt"/>
			<fileset dir="data/xliff-lib" includes="**/notice.txt"/>
		</copy>

		<!-- Clean up -->
		<delete includeEmptyDirs="true" failonerror="false">
			<fileset dir="${tmp}"/>
		</delete>

		<!-- Make ZIP of the package -->
		<zip destfile="${done}/${zipName}.zip" basedir="${dist_xlifflib}"/>

	</target>
	
	<target name="all" depends="init, makeJars, makeLynx"/>

</project>
