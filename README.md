The **Okapi XLIFF Toolkit** is a library that implements a set of Java components to create, read and manipulate XLIFF 2 documents.

XLIFF 2 is being developed by the [OASIS XLIFF TC](http://www.oasis-open.org/committees/tc_home.php?wg_abbrev=xliff). You can find more information [here](https://wiki.oasis-open.org/xliff/).
The lastest published version of XLIFF 2 can be found at http://docs.oasis-open.org/xliff/xliff-core/v2.0/xliff-core-v2.0.html
There is an introduction to XLIFF 2.0 [in this Multilingual article](https://multilingual.com/all-articles/?art_id=2139).

 * [ ![Download](https://api.bintray.com/packages/okapi/Distribution/Okapi_XLIFF_Toolkit/images/download.svg)](https://bintray.com/okapi/Distribution/Okapi_XLIFF_Toolkit/_latestVersion)
The latest stable version of the XLIFF Toolkit is at https://bintray.com/okapi/Distribution/Okapi_XLIFF_Toolkit
 * The latest release Maven artifact is here: [http://repository-okapi.forge.cloudbees.com/release/](http://repository-okapi.forge.cloudbees.com/release/)
 * Get the latest snapshot (development version) here: [okapi-xliffLib_all-platforms_<version>.zip](http://okapiframework.org/snapshots/)
 * the latest snapshot Maven artifact is here: [http://repository-okapi.forge.cloudbees.com/snapshot/](http://repository-okapi.forge.cloudbees.com/snapshot/)
 * On-line validation of XLIFF 2 documents: **http://okapi-lynx.appspot.com/validation**
 * You can now edit XLIFF 2 files in [OmegaT](http://www.omegat.org/en/omegat.html) using the [Okapi Filter Plugin](http://okapiframework.org/wiki/index.php?title=Okapi_Filters_Plugin_for_OmegaT#Support_for_XLIFF_2).
 * Get a [set of valid and invalid XLIFF 2 documents](https://tools.oasis-open.org/version-control/browse/wsvn/xliff/trunk/xliff-20/test-suite/) to test if a XLIFF 2 user agent is reading documents properly.
 * Read the **[Getting Started guide](https://bitbucket.org/okapiframework/xliff-toolkit/wiki)**.
 * Read the [Java Documentation](http://okapiframework.org/xlifflib/javadoc/) of the library.
 * See also the tentative / prototyping object model for XLIFF and JSON serialization for XLIFF being worked on here: https://www.oasis-open.org/committees/xliff-omos.

![CloudBees: https://okapi.ci.cloudbees.com/](http://web-static-cloudfront.s3.amazonaws.com/images/badges/BuiltOnDEV.png)