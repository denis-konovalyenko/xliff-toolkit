package net.sf.okapi.lib.xliff2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.sf.okapi.lib.xliff2.test.U;

import org.junit.Test;

public class URIPrefixesTest {

	private final String root = U.getParentDir(this, "/example.xlf");

	@Test
	public void testResolveWithDefaults () {
		URIPrefixes upr = new URIPrefixes();
		assertEquals(Const.NS_XLIFF_GLOSSARY20, upr.resolve("gls").get(0));
		assertEquals(Const.NS_XLIFF_MATCHES20, upr.resolve("mtc").get(0));
		assertEquals("urn:oasis:names:tc:xliff:resourcedata:2.0", upr.resolve("res").get(0));
	}

	@Test
	public void testResolveUnknown () {
		assertEquals(null, new URIPrefixes().resolve("xyz"));
		assertEquals(null, new URIPrefixes().resolve("GLS"));
	}
	
	@Test
	public void testCustomPrefixes () {
		URIPrefixes upr = new URIPrefixes(new File(root+"/extra-prefixes.properties"));
		List<String> list = upr.resolve("tbx");
		assertEquals(1, list.size());
		assertEquals("urn:iso:std:iso:30042:ed-1:v1:en", list.get(0));
		// Additions
		list = upr.resolve("gls");
		assertEquals(2, list.size());
		assertEquals(Const.NS_XLIFF_GLOSSARY20, list.get(0));
		assertEquals("testGLSv2.x", list.get(1));
	}

	@Test
	public void testManualCustomPrefixes () {
		URIPrefixes upr = new URIPrefixes();
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("myURI1", "p1");
		map.put("myURITwo", "p2");
		map.put("myURI1bis", "p1");
		upr.add(map);
		List<String> list = upr.resolve("p2");
		assertEquals("myURITwo", list.get(0));
		list = upr.resolve("p1");
		assertEquals(2, list.size());
		assertEquals("myURI1", list.get(0));
		assertEquals("myURI1bis", list.get(1));
	}
	
	@Test (expected = XLIFFException.class)
	public void testBadCustomPrefix_TooShort () {
		URIPrefixes upr = new URIPrefixes(new File(root+"/bad-prefix-tooshort.properties"));
		upr.resolve("xyz"); // Trigger the load
	}

	@Test (expected = XLIFFException.class)
	public void testBadCustomPrefix_NotNMTOKEN () {
		URIPrefixes upr = new URIPrefixes(new File(root+"/bad-prefix-notnmtoken.properties"));
		upr.resolve("xyz"); // Trigger the load
	}

	@Test
	public void testBadCustomPrefix_DuplicatedURIs () {
		URIPrefixes upr = new URIPrefixes(new File(root+"/extra-prefixes.properties"));
		// In case of a URI assigned to two different prefixes, the last one wins
		List<String> uris = upr.resolve("notTakenIntoAccount"); // Trigger the load
		assertEquals(null, uris);
		uris = upr.resolve("my");
		assertEquals("myNS", uris.get(0));
	}

	@Test
	public void testRegistryLoader () {
		URIPrefixes upr = new URIPrefixes();
		Map<String, List<String>> map = upr.loadFromRegistry();
		assertTrue(map.size()>0);
		List<String> uris = map.get("my");
		assertTrue(null!=uris);
		assertEquals(1, uris.size());
		assertEquals("http://example.org/myXLIFFExtensionURI", uris.get(0));
	}

}
